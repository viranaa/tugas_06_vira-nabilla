Feature: SauceDemo Product Filtering Functionality

  Scenario: Filter Products by Price
    Given I am logged in to the SauceDemo website
    When I navigate to the "Products" page
    And I select the "Price (low to high)" filter
    Then I should see the products in the correct order

  Scenario: Filter Products by Name
    Given I am logged in to the SauceDemo website
    When I navigate to the "Products" page
    And I select the "Name (A to Z)" filter
    Then I should see the products in the correct order