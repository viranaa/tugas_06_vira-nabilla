Feature: SauceDemo Shopping Functionality

  Scenario: Add Item to Cart
    Given logged in to the SauceDemo website
    When navigate to the "Products" page
    And I add a product to the cart
    Then the item should be added to the cart successfully

  Scenario: Remove Item from Cart
    Given logged in to the SauceDemo website
    When navigate to the "Products" page
    And I add a product to the cart
    And I go to the shopping cart
    And I remove the product from the cart
    Then the item should be removed from the cart successfully