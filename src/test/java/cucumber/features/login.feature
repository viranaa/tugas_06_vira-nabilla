Feature: Login functionality

  Scenario: Successful Login
    Given SauceDemo login page
    When I enter standard_user in username field
    And I enter secret_sauce in password field
    And I click the Login button
    Then I should be logged in and redirected to the products page

  Scenario: Invalid Password
    Given SauceDemo login page
    When I enter standard_user in username field
    And I enter invalid_pass in password field
    And I click the Login button
    Then I should see an error message indicating that the password is incorrect
