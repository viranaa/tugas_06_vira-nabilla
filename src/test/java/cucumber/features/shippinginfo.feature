Feature: Shipping Information Field Verification

  Scenario: Enter Valid Shipping Information
    Given Shipping Information page
    When I am on the shipping information page
    And I enter valid shipping information
    And I click the Continue button
    Then I should be able to proceed to the payment page

  Scenario: Enter Incomplete Shipping Information
    Given Shipping Information page
    And I leave all required shipping information fields empty
    And I click the Continue button
    Then I should see an error message indicating that all required fields must be filled