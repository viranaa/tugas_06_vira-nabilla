package cucumber.stepDef;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class ShippingInfo {
    private WebDriver driver;
    String baseUrl = "https://saucedemo.com/";

    @Given("Shipping Information page")
    public void shippingInformationPage() {
        driver = new ChromeDriver();
        driver.get(baseUrl);
        WebElement usernameField = driver.findElement(By.id("user-name"));
        usernameField.sendKeys("standard_user");
        WebElement passwordField = driver.findElement(By.id("password"));
        passwordField.sendKeys("secret_sauce");
        WebElement loginButton = driver.findElement(By.id("login-button"));
        loginButton.click();
        WebElement AddtocartButton = driver.findElement (By.xpath("//*[@data-test=\"add-to-cart-sauce-labs-onesie\"]"));
        AddtocartButton.click();
        WebElement cartButton = driver.findElement(By.className("shopping_cart_link"));
        cartButton.click();
        WebElement CheckoutButton = driver.findElement (By.xpath("//*[@data-test=\"checkout\"]"));
        CheckoutButton.click();
    }

    @When("I am on the shipping information page")
    public void iAmOnTheShippingInformationPage() {
        String currentUrl = driver.getCurrentUrl();
        Assert.assertEquals("https://www.saucedemo.com/checkout-step-one.html", currentUrl);
    }

    @And("I enter valid shipping information")
    public void iEnterValidShippingInformation() {
        WebElement firstnameField = driver.findElement(By.xpath("//*[@data-test=\"firstName\"]"));
        firstnameField.sendKeys("Vira");
        WebElement lastnameField = driver.findElement(By.xpath("//*[@data-test=\"lastName\"]"));
        lastnameField.sendKeys("Nabilla");
        WebElement zipcodeField = driver.findElement(By.xpath("//*[@data-test=\"postalCode\"]"));
        zipcodeField.sendKeys("123456");
    }

    @And("I click the Continue button")
    public void iClickTheContinueButton() {
        WebElement continueButton = driver.findElement(By.xpath("//*[@data-test=\"continue\"]"));
        continueButton.click();
    }

    @Then("I should be able to proceed to the payment page")
    public void iShouldBeAbleToProceedToThePaymentPage() {
        String currentUrl = driver.getCurrentUrl();
        Assert.assertEquals("https://www.saucedemo.com/checkout-step-two.html", currentUrl);
    }

    @And("I leave all required shipping information fields empty")
    public void iLeaveAllRequiredShippingInformationFieldsEmpty() {
        WebElement firstnameField = driver.findElement(By.xpath("//*[@data-test=\"firstName\"]"));
        firstnameField.sendKeys("");
        WebElement lastnameField = driver.findElement(By.xpath("//*[@data-test=\"lastName\"]"));
        lastnameField.sendKeys("");
        WebElement zipcodeField = driver.findElement(By.xpath("//*[@data-test=\"postalCode\"]"));
        zipcodeField.sendKeys("");
    }

    @Then("I should see an error message indicating that all required fields must be filled")
    public void iShouldSeeAnErrorMessageIndicatingThatAllRequiredFieldsMustBeFilled() {
        List<WebElement> errorMessage = driver.findElements(By.xpath("//*[@data-test=\"error\"]"));
        Assert.assertEquals(1, errorMessage.size());
        driver.close();
    }
}
