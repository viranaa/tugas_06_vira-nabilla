package cucumber.stepDef;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class Filter{
    private WebDriver driver;
    String baseUrl = "https://saucedemo.com/";

    @Given("I am logged in to the SauceDemo website")
    public void i_am_logged_in_to_the_sauce_demo_website() {
        // Initialize WebDriver, navigate to the website, and perform any necessary setup
        driver = new ChromeDriver();
        driver.get(baseUrl);
        WebElement usernameField = driver.findElement(By.id("user-name"));
        usernameField.sendKeys("standard_user");
        WebElement passwordField = driver.findElement(By.id("password"));
        passwordField.sendKeys("secret_sauce");
        WebElement loginButton = driver.findElement(By.id("login-button"));
        loginButton.click();
    }

    @When("I navigate to the {string} page")
    public void i_navigate_to_the_page(String pageName) {
        // Implement navigation to the "Products" page
        // Add similar steps for navigating to other pages
        if (pageName.equals("Products")) {
            List<WebElement> productsLink = driver.findElements(By.xpath("//*[@class=\"title\"]"));
            Assert.assertEquals(1, productsLink.size());
        }
    }

    @When("I select the {string} filter")
    public void i_select_the_filter(String filterName) {
        // Implement selecting filters like "Price (low to high)" or "Name (A to Z)"
        // Add similar steps for selecting other filters
        WebElement order = driver.findElement(By.xpath("//*[@data-test=\"product_sort_container\"]"));
        if (filterName.equals("Price (low to high)")) {
            WebElement priceLowToHighFilter = order.findElement(By.xpath("//*[@value=\"lohi\"]"));
            priceLowToHighFilter.click();
        } else if (filterName.equals("Name (A to Z)")) {
            WebElement nameAToZFilter = driver.findElement(By.xpath("//*[@value=\"az\"]"));
            nameAToZFilter.click();
        }
    }

    @Then("I should see the products in the correct order")
    public void i_should_see_the_products_in_the_correct_order() {
        // Implement verification for the correct order
        // Verify that the products are displayed in the expected order
    }

    @Then("I close the browser")
    public void i_close_the_browser() {
        // Implement closing the browser to end the test
        driver.close();
    }
}
