package cucumber.stepDef;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class Shopping {
    private WebDriver driver;
    String baseUrl = "https://saucedemo.com/";

    @Given("logged in to the SauceDemo website")
    public void logged_in_to_the_sauce_demo_website() {
        // Initialize WebDriver, navigate to the website, and perform any necessary setup
        driver = new ChromeDriver();
        driver.get(baseUrl);
        WebElement usernameField = driver.findElement(By.id("user-name"));
        usernameField.sendKeys("standard_user");
        WebElement passwordField = driver.findElement(By.id("password"));
        passwordField.sendKeys("secret_sauce");
        WebElement loginButton = driver.findElement(By.id("login-button"));
        loginButton.click();
    }

    @When("navigate to the {string} page")
    public void navigate_to_the_page(String pageName) {
        // Implement navigation to the "Products" page
        // Add similar steps for navigating to other pages
        String currentUrl = driver.getCurrentUrl();
        Assert.assertEquals("https://www.saucedemo.com/inventory.html", currentUrl);
        if (pageName.equals("Sauce Labs Backpack")) {
            List<WebElement> productsLink = driver.findElements(By.xpath("//*[text()=\"Sauce Labs Backpack\"]"));
            Assert.assertEquals(1, productsLink.size());
        }
    }

    @And("I add a product to the cart")
    public void i_add_a_product_to_the_cart() {
        WebElement AddtocartButton = driver.findElement (By.xpath("//*[@data-test=\"add-to-cart-sauce-labs-backpack\"]"));
        AddtocartButton.click();
    }

    @And("I go to the shopping cart")
    public void i_go_to_the_shopping_cart() {
        WebElement cartButton = driver.findElement(By.className("shopping_cart_link"));
        cartButton.click();
        String currentUrl = driver.getCurrentUrl();
        Assert.assertEquals("https://www.saucedemo.com/cart.html", currentUrl);
    }

    @Then("the item should be added to the cart successfully")
    public void the_item_should_be_added_to_the_cart_successfully() {
        List<WebElement> item = driver.findElements(By.xpath("//*[text()=\"Sauce Labs Backpack\"]"));
        Assert.assertEquals(1, item.size());
    }

    @And("I remove the product from the cart")
    public void i_remove_the_product_from_the_cart() {
        WebElement RemoveButton = driver.findElement (By.xpath("//*[@data-test=\"remove-sauce-labs-backpack\"]"));
        RemoveButton.click();
    }

    @Then("the item should be removed from the cart successfully")
    public void the_item_should_be_removed_from_the_cart_successfully() {
        List<WebElement> item = driver.findElements(By.xpath("//*[text()=\"Sauce Labs Backpack\"]"));
        Assert.assertEquals(0, item.size());
    }
    @Then("close the browser")
    public void close_the_browser() {
        // Implement closing the browser to end the test
        driver.close();
    }
}
