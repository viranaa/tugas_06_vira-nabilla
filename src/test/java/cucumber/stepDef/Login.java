package cucumber.stepDef;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.junit.Assert;

import java.util.List;

public class Login {
    WebDriver driver;
    String baseUrl = "https://saucedemo.com/";

    @Given("SauceDemo login page")
    public void sauce_demo_login_page() {
        driver = new ChromeDriver();
        driver.get(baseUrl);

        String LoginpageAssert = driver.findElement(By.xpath("//div[contains(text(), 'Swag Labs')]")).getText();
        Assert.assertEquals(LoginpageAssert, "Swag Labs");
    }
    @When("I enter standard_user in username field")
    public void i_enter_standard_user_in_username_field() {
        WebElement usernameField = driver.findElement(By.id("user-name"));
        usernameField.sendKeys("standard_user");
    }

    @When("I enter secret_sauce in password field")
    public void i_enter_secret_sauce_in_password_field() {
        WebElement passwordField = driver.findElement(By.id("password"));
        passwordField.sendKeys("secret_sauce");
    }

    @When("I enter invalid_pass in password field")
    public void i_enter_invalid_pass_in_password_field() {
        WebElement passwordField = driver.findElement(By.id("password"));
        passwordField.sendKeys("invalid_pass");
    }

    @When("I click the Login button")
    public void i_click_the_login_button() {
        WebElement loginButton = driver.findElement(By.id("login-button"));
        loginButton.click();
    }

    @Then("I should be logged in and redirected to the products page")
    public void i_should_be_logged_in_and_redirected_to_the_products_page() {
        String currentUrl = driver.getCurrentUrl();
        Assert.assertEquals("https://www.saucedemo.com/inventory.html", currentUrl);
        driver.close();
    }

    @Then("I should see an error message indicating that the password is incorrect")
    public void i_should_see_an_error_message_indicating_that_the_password_is_incorrect() {
        List<WebElement> errorPopup = driver.findElements(By.xpath("//*[@class=\"error\"]"));
        Assert.assertEquals(0, errorPopup.size());
        driver.close();
    }
}
