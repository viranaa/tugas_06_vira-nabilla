import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class TestMain {
    @Test
    public void Test(){
        WebDriver wd;
        String baseurl = "https://saucedemo.com";
        ChromeOptions opts = new ChromeOptions();
        wd = new ChromeDriver(opts);
        wd.manage().window().maximize();
        wd.get(baseurl);
        wd.close();
    }
}
